﻿using System.Collections.Generic;

public enum DatabaseRootId
{
    Testing = 0,
    Public,
}

public enum DataType
{   
    Missions = 0,
    PlayerDatas,
    PlayerCount,
}

public enum MissionType
{
    Sport = 0,
    Relationship,
}

public static class DatabaseRoot
{
    public static Dictionary<DatabaseRootId, string> DataRootRef = new Dictionary<DatabaseRootId, string>()
    {
        { DatabaseRootId.Testing, "testing" },
        { DatabaseRootId.Public, "public" },
    };

    public static Dictionary<DataType, string> DataTypeRef = new Dictionary<DataType, string>()
    {
        { DataType.Missions, "Missions"},
        { DataType.PlayerDatas, "PlayerDatas"},
        { DataType.PlayerCount, "PlayerCount"},
    };

    public static Dictionary<MissionType, string> MissionRootRef = new Dictionary<MissionType, string>()
    {
        { MissionType.Sport, "Sports"},
        { MissionType.Relationship, "Relationship"},
    };

}
