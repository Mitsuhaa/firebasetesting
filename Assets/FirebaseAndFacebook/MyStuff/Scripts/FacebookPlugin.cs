﻿using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FacebookPlugin : MonoBehaviour
{
    public delegate void LoginCallback(bool success);
    public static System.Action<LoginCallback, string> onFacebookLogin = delegate { };

    // Awake function from Unity's MonoBehavior
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            DebugLog("Failed to Initialize the Facebook SDK");
        }
    }

    public static void SignInWithFacebook()
    {
        SignInFacebook(result =>
        {
            if (result)
            {
                DebugLog("Login Succesful");
            }
            else
            {
                DebugLog("Login Failed");
            }

            onFacebookLogin = delegate { };
        });
    }

    //  Real sign in
    private static void SignInFacebook(LoginCallback callback = null)
    {
        FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, (loginResult) =>
        {
            if (!string.IsNullOrEmpty(HandleFBResult(loginResult)))
            {
                FB.Mobile.RefreshCurrentAccessToken((accessTokenResult) =>
                {
                    string token = HandleFBResult(accessTokenResult);

                    if (token != null)
                    {
                        if (accessTokenResult.ResultDictionary.ContainsKey("access_token"))
                        {
                            string accessToken = accessTokenResult.ResultDictionary["access_token"].ToString();
                            onFacebookLogin(callback, accessToken);
                        }
                    }
                });
            }
            else
            {
                if (callback != null)
                {
                    callback(false);
                }
            }
        });
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            DebugLog(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                DebugLog(perm);
            }
        }
        else
        {
            DebugLog("User cancelled login");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    #region Utilities

    private static string HandleFBResult(IResult result)
    {
        if (result == null)
        {
            DebugLog("Facebook Login: Null Response");
            return null;
        }

        // Some platforms return the empty string instead of null.
        if (!string.IsNullOrEmpty(result.Error))
        {
            DebugLog("Facebook Login: Error Response:" + result.Error);
            return null;
        }
        else if (result.Cancelled)
        {
            DebugLog("Facebook Login: Cancelled Response:" + result.RawResult);
            return null;
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            DebugLog("Facebook Login: Success Response:" + result.RawResult);
        }
        else
        {
            DebugLog("Facebook Login: Empty Response");
        }

        return result.RawResult;
    }

    private static void DebugLog(string output)
    {
#if DEBUG_LOG
        Debug.Log(output);
#endif
    }

    #endregion
}
