﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "MissionDatas", menuName = "Missions")]
public class MissionDataObject : ScriptableObject
{
    public string type;
    public MissionDataBase[] missionData;

    public List<Dictionary<object, object>> ToVData()
    {
        List<Dictionary<object, object>> result = new List<Dictionary<object, object>>();

        for (int i = 0; i < missionData.Length; ++i)
        {
            result.Add(missionData[i].ToVData());
        }

        return result;
    }
}
