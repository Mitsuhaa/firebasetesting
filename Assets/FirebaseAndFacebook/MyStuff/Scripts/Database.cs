﻿using Firebase.Database;
using System;
using UnityEngine;

public class Database : MonoBehaviour
{
    private static Database _instance;
    public static Database Instance { get { return _instance; } }

    private int playerCount = 0;
    public int PlayerCount { get { return playerCount; } }

    public Action onDatabaseLoaded = delegate { };
    public Action<DataSnapshot> onPlayerDataLoaded = delegate { };
    public Action<DataSnapshot> onMissionDataLoaded = delegate { };
    public Action<string> onPlayerDataSaved = delegate { };

    private bool testing = false;
    private DatabaseRootId rootId;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        if (_instance == this)
        {
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        rootId = DatabaseRootId.Public;
#if TESTING
        rootId = DatabaseRootId.Testing;
#endif

        FirebasePlugin.onDatabaseLoaded += onDatabaseLoaded;
        FirebasePlugin.onDatabaseLoaded += LoadPlayerCount;
        FirebasePlugin.LoadDatabase();
    }

    public void LoadPlayerData()
    {
        FirebasePlugin.onDataLoaded = onPlayerDataLoaded;
        FirebasePlugin.LoadData(MakeString(rootId, DataType.PlayerDatas));
    }

    private void LoadPlayerCount()
    {
        FirebasePlugin.onDataLoaded += LoadDataToPlayerCount;
        FirebasePlugin.LoadData(MakeString(rootId, DataType.PlayerCount));
    }

    private void LoadDataToPlayerCount(DataSnapshot snapshot)
    {
        playerCount = int.Parse(snapshot.Value.ToString());
        FirebasePlugin.onDatabaseLoaded -= LoadPlayerCount;
    }

    public string SavePlayerData(bool isDefault, string playerIDKeyString, object data)
    {
        string playerRef = MakeString(rootId, DataType.PlayerDatas);
        if (isDefault)
        {
            playerIDKeyString += FirebasePlugin.PushAndGetKey(playerRef);
            ++playerCount;
            FirebasePlugin.SaveData(MakeString(rootId, DataType.PlayerCount), playerCount);
        }

        playerRef += "/" + playerIDKeyString;

        FirebasePlugin.SaveData(playerRef, data);

        return playerIDKeyString;
    }

    public void LoadMissionsData(MissionType mission)
    {
        FirebasePlugin.onDataLoaded = onMissionDataLoaded;

        switch (mission)
        {
            case MissionType.Sport:
            case MissionType.Relationship:
                FirebasePlugin.LoadData(MakeString(rootId, DataType.Missions, mission));
                break;

            default:
                DebugLog("Incorrect mission value.");
                break;
        }
    }

    public void SaveMissionData(MissionType mission, object data)
    {
        FirebasePlugin.SaveData(MakeString(rootId, DataType.Missions, mission), data);
    }

    #region Utilities

    private static void DebugLog(string output)
    {
#if DEBUG_LOG
        Debug.Log(output);
#endif
    }

    private string MakeString(DatabaseRootId rootId, DataType dataType)
    {
        return DatabaseRoot.DataRootRef[rootId] + "/" + DatabaseRoot.DataTypeRef[dataType];
    }

    private string MakeString(DatabaseRootId rootId, DataType dataType, MissionType missionType)
    {
        return DatabaseRoot.DataRootRef[rootId] + "/" + DatabaseRoot.DataTypeRef[dataType] + "/" + DatabaseRoot.MissionRootRef[missionType];
    }

    #endregion
}
