﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MissionDataController : MonoBehaviour
{
    private static MissionDataController _instance;
    public static MissionDataController Instance { get { return _instance; } }

    private List<MissionDataBase> missionsData = new List<MissionDataBase>();
    public List<MissionDataBase> MissionData { get { return missionsData; } }

    private List<Texture2D> textures = new List<Texture2D>();
    public List<Texture2D> Textures { get { return textures; } }

    [SerializeField] private MissionDataObject[] missionObjects;

    private string photoPrefix = "gs://testfirebase-397df.appspot.com/";

    private MissionType missionIndex, endOfMissionType = MissionType.Relationship;
    private int missionsToLoad = 5;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        if (_instance == this)
        {
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void LoadMission(int missionType)
    {
        Database.Instance.onMissionDataLoaded = LoadMissionData;
        Database.Instance.LoadMissionsData(IntToMissionType(missionType));
    }

    private void LoadMissionData(DataSnapshot snapshot)
    {
        List<object> tempList = snapshot.Value as List<object>;
        List<MissionDataBase> tempMissionsDataList = new List<MissionDataBase>();

        for (int i = 0; i < tempList.Count; ++i)
        {
            MissionDataBase tempMissionsData = new MissionDataBase(tempList[i] as Dictionary<string, object>);
            tempMissionsDataList.Add(tempMissionsData);
        }

        for (int i = 0; i < missionsToLoad; ++i)
        {
            int index = Random.Range(0, tempMissionsDataList.Count);
            missionsData.Add(tempMissionsDataList[index]);
            tempMissionsDataList.RemoveAt(index);
            if (tempMissionsDataList[index].photoUrl != "")
            {
                FirebasePlugin.DownloadImage(photoPrefix + tempMissionsDataList[index].photoUrl, url =>
                {
                    DatabaseRuntime.Instance.AddEntry(StartImageCoroutine, url);
                });
            }
            else
            {
                textures.Add(null);
            }
        }

        FirebasePlugin.onDataLoaded = delegate { };
    }

    public void LoadAllMission()
    {
        Database.Instance.onMissionDataLoaded = LoadAllMissionData;
        missionIndex = MissionType.Sport;
        Database.Instance.LoadMissionsData(missionIndex);
    }

    private void LoadAllMissionData(DataSnapshot snapshot)
    {
        List<object> tempList = snapshot.Value as List<object>;
        missionObjects[(int)missionIndex].missionData = new MissionDataBase[tempList.Count];

        for (int i = 0; i < tempList.Count; ++i)
        {
            MissionDataBase tempMissionsData = new MissionDataBase(tempList[i] as Dictionary<string, object>);
            missionObjects[(int)missionIndex].type = missionIndex.ToString();
            missionObjects[(int)missionIndex].missionData[i] = tempMissionsData;
        }

        if (missionIndex < endOfMissionType) {
            ++missionIndex;
            DatabaseRuntime.Instance.AddEntry(delegate ()
            {
                Database.Instance.LoadMissionsData(missionIndex);
            });
        }
        else
        {
            Database.Instance.onMissionDataLoaded = delegate { };
        }
    }

    public void SaveAllMission()
    {
        for (int i = 0; i < missionObjects.Length; ++i)
        {
            Database.Instance.SaveMissionData(IntToMissionType(i), missionObjects[i].ToVData()); 
        }
    }

    private void StartImageCoroutine(string url)
    {
        StartCoroutine(ImageLoading(url));
    }

    IEnumerator ImageLoading(string url)
    {
        Debug.Log("Downloading");
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            textures.Add(myTexture);
        }
        Debug.Log("Finished");
    }

    private MissionType IntToMissionType(int val)
    {
        return (MissionType)val;
    }
}
