﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MissionDataBase
{
    public string question;
    public List<string> answers;
    public string photoUrl = null;

    public Dictionary<object, object> ToVData()
    {
        Dictionary<object, object> result = new Dictionary<object, object>();

        result.Add("question", question);
        result.Add("answers", answers);
        result.Add("photoUrl", photoUrl);

        return result;
    }

    public MissionDataBase() { }

    public MissionDataBase(Dictionary<string, object> dict)
    {
        question = dict["question"].ToString();
        answers = new List<string>();
        for (int i = 0; i < (dict["answers"] as List<object>).Count; ++i)
        {
            answers.Add((dict["answers"] as List<object>)[i].ToString());
        }
        photoUrl = dict["photoUrl"].ToString();
    }
}


