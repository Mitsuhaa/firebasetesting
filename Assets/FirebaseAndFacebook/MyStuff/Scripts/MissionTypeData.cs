﻿using UnityEngine;

[System.Serializable]
public class MissionTypeData
{
    public MissionType type;
    public string title;
    public string desc;
    public Sprite icon;
}

[System.Serializable]
public class MissionTypeDB
{
    public MissionType type;
    public string dbRef;
}
