﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataBase<T>
{
    public T property;

    public PlayerDataBase() 
    {
        property = default(T);
    }

}

[System.Serializable]
public class PlayerDataFloat : PlayerDataBase<float> { };

[System.Serializable]
public class PlayerDataInt : PlayerDataBase<int> { };

[System.Serializable]
public class PlayerDataCloths : PlayerDataBase<Cloths> { };

[System.Serializable]
public class Cloths
{
    public int[] iDs;

    public Cloths(int value)
    {
        iDs = new int[value];
    }

    public List<object> ToVData()
    {
        List<object> result = new List<object>();

        for (int i = 0; i < iDs.Length; ++i)
        {
            result.Add(iDs[i]);
        }

        return result;
    }

    public Cloths(List<object> data)
    {
        iDs = new int[data.Count];

        for (int i = 0; i < data.Count; ++i)
        {
            iDs[i] = int.Parse(data[i].ToString());
        }
    }
}