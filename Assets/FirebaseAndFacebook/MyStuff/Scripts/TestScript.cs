﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{
    public Text text;
    public RawImage image;
    int missionIndex = 0;
    PlayerDataController playerDataController;
    PlayerDatas runtimeDatas;

    // Start is called before the first frame update
    void Start()
    {
        playerDataController = PlayerDataController.Instance;
        PlayerDataController.RuntimeDatasChanged += UpdateText;
    }

    private void UpdateText()
    {
        PlayerDatas runtimeData = PlayerDataController.Instance.RuntimeDatas;
        text.text = "Player ID : " + runtimeData.playerID + "\nPlayer Coins : " + runtimeData.playerCoins + "\nPlayer Experience : " + runtimeData.playerExperience + "\nPlayer Stamina: " + runtimeData.playerStamina + "\nCloths 1: " + runtimeData.playerCloths.iDs[0];
    }

    public void DecreaseMoney()
    {
        runtimeDatas = playerDataController.RuntimeDatas;
        PlayerDataController.IncreaseInt(ref runtimeDatas.playerCoins, -10);
    }

    public void LoginFacebook()
    {
        FirebasePlugin.LoginWithFacebook();
    }

    public void LinkFacebook()
    {
        FirebasePlugin.LinkWithFacebook();
    }

    public void UnlinkFacebook()
    {
        FirebasePlugin.UnlinkFacebook();
    }

    public void NextQuestion()
    {
        List<MissionDataBase> missionsData = MissionDataController.Instance.MissionData;
        List<Texture2D> textures = MissionDataController.Instance.Textures;

        text.text = "Question: " + missionsData[missionIndex].question + "\nAnswer 1: " + missionsData[missionIndex].answers[0] + "\tAnswer 2: " + missionsData[missionIndex].answers[1];
        image.texture = textures[missionIndex];
        ++missionIndex;
    }

    public void GetServerTime()
    {
        FirebasePlugin.GetCurrentTime();
    }
}
