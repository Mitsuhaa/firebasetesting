﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using Firebase.Functions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public static class FirebasePlugin
{
    public static Action onDatabaseLoaded = delegate { };
    public static Action<DataSnapshot> onDataLoaded = delegate { };
    public static Action onCredentialAlreadyInUse = delegate { };

    public static void LoadDatabase()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            DependencyStatus dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                FirebaseApp firebaseApp = FirebaseApp.DefaultInstance;

                DatabaseRuntime.Instance.AddEntry(delegate ()
                {
                    //OnDatabaseLoaded();
                });
            }
            else
            {
                DebugLog(String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }

    public static void LoadData(string databaseReference)
    {
        DataSnapshot snapshot = null;

        FirebaseDatabase.DefaultInstance.GetReference(databaseReference).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                DebugLog(task.ToString());
            }
            else if (task.IsCompleted)
            {
                snapshot = task.Result;
                DatabaseRuntime.Instance.AddEntry(OnDataLoaded, snapshot);
            }
        });
    }

    public static void SaveData(string databaseReference, object data)
    {
        FirebaseDatabase.DefaultInstance.GetReference(databaseReference).SetValueAsync(data)
                 .ContinueWithOnMainThread(task =>
                 {
                     if (task.Exception != null)
                     {
                         DebugLog(task.Exception.ToString());
                     }
                     else if (task.IsCompleted)
                     {
                         DebugLog("Transaction complete.");
                     }
                     else
                     {
                         DebugLog(task.IsFaulted.ToString());
                     }
                 });
    }

    public static string PushAndGetKey(string databaseReference)
    {
        return FirebaseDatabase.DefaultInstance.GetReference(databaseReference).Push().Key;
    }

    public static void LoginAsAnon()
    {
        FirebaseAuth.DefaultInstance.SignInAnonymouslyAsync().ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                DebugLog("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                DebugLog("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }

            FirebaseUser newUser = task.Result;
            DebugLog(string.Format("User signed in successfully: ({0}) ({1})",
                newUser.DisplayName, newUser.UserId));
        });
    }

    public static void LoginWithFacebook()
    {
        FacebookPlugin.onFacebookLogin += SignInWithFacebook;
        FacebookPlugin.SignInWithFacebook();
    }

    public static void LinkWithFacebook()
    {
        FacebookPlugin.onFacebookLogin += LinkToFacebook;
        FacebookPlugin.SignInWithFacebook();
    }

    private static void SignInWithFacebook(FacebookPlugin.LoginCallback callback, string accessToken)
    {
        FirebaseAuth firebaseAuth = FirebaseAuth.GetAuth(FirebaseApp.DefaultInstance);

        Credential cred = FacebookAuthProvider.GetCredential(accessToken);

        firebaseAuth.SignInWithCredentialAsync(cred).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                DebugLog("SignInWithCredentialAsync was canceled.");
                if (callback != null)
                {
                    callback(false);
                }
            }
            if (task.IsFaulted)
            {
                DebugLog("SignInWithCredentialAsync encountered an error: " + task.Exception);
                if (callback != null)
                {
                    callback(false);
                }
            }

            FirebaseUser newUser = task.Result;
            DebugLog(string.Format("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId));
            if (callback != null)
            {
                callback(true);
            }

        });
    }

    private static void LinkToFacebook(FacebookPlugin.LoginCallback callback, string accessToken)
    {
        FirebaseAuth firebaseAuth = FirebaseAuth.GetAuth(FirebaseApp.DefaultInstance);

        Credential cred = FacebookAuthProvider.GetCredential(accessToken);

        firebaseAuth.CurrentUser.LinkWithCredentialAsync(cred).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                DebugLog("LinkWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                DebugLog(GetErrorMessage(task.Exception));
                return;
            }

            FirebaseUser newUser = task.Result;
            DebugLog(string.Format("Credentials successfully linked to Firebase user: {0} ({1})",
                newUser.DisplayName, newUser.UserId));
        });
    }

    public static void UnlinkFacebook()
    {
        FirebaseAuth firebaseAuth = FirebaseAuth.GetAuth(FirebaseApp.DefaultInstance);

        firebaseAuth.CurrentUser.UnlinkAsync("facebook.com").ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                DebugLog("UnlinkAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                DebugLog("UnlinkAsync encountered an error: " + task.Exception);
                return;
            }

            // The user has been unlinked from the provider.
            FirebaseUser newUser = task.Result;

            DebugLog(string.Format("Credentials successfully unlinked from user: {0} ({1})",
                newUser.DisplayName, newUser.UserId));
        });
    }

    private static void FBLoginCallback(bool result)
    {
        if (result)
        {
            DebugLog("Login successful");
        }
        else
        {
            DebugLog("Login failed");
        }
    }

    public static void DownloadImage(string url, Action<string> callback)
    {
        // Create a reference from a Google Cloud Storage URI
        Firebase.Storage.StorageReference gs_reference =
          Firebase.Storage.FirebaseStorage.DefaultInstance.GetReferenceFromUrl(url);

        gs_reference.GetDownloadUrlAsync().ContinueWith((Task<Uri> task) =>
        {
            if (!task.IsFaulted && !task.IsCanceled)
            {
                //Debug.Log("Download URL: " + task.Result);
                callback(task.Result.ToString());
            }
        });
    }

    private static void SaveMissionData()
    {
        List<Dictionary<object, object>> missionsData = new List<Dictionary<object, object>>();

        MissionDataBase temp = new MissionDataBase();
        temp.question = "What is the world record of 100m sprint?";
        temp.answers = new List<string>();
        temp.answers.Add("9.58 seconds");
        temp.answers.Add("9.79 seconds");
        temp.answers.Add("9.12 seconds");
        temp.answers.Add("8.97 seconds");
        temp.photoUrl = "";

        missionsData.Add(temp.ToVData());

        temp.question = "Highest scorer in nba history";
        temp.answers = new List<string>();
        temp.answers.Add("michael jordan");
        temp.answers.Add("kobe bryant");
        temp.answers.Add("lebron james");
        temp.answers.Add("xxxx");
        temp.photoUrl = "gs://testfirebase-397df.appspot.com/WPVG_icon_2016.svg.png";

        missionsData.Add(temp.ToVData());
        temp.question = "3";
        missionsData.Add(temp.ToVData());
        temp.question = "4";
        missionsData.Add(temp.ToVData());
        temp.question = "5";
        missionsData.Add(temp.ToVData());
        temp.question = "7";
        missionsData.Add(temp.ToVData());
        temp.question = "8";
        missionsData.Add(temp.ToVData());
        temp.question = "9";
        missionsData.Add(temp.ToVData());

        SaveData("testing/Missions/Sports", missionsData);

        temp.question = "This is relationship";
        missionsData.Add(temp.ToVData());

        SaveData("testing/Missions/Relationship", missionsData);
    }

    private static void OnDataLoaded(DataSnapshot snapshot)
    {
        onDataLoaded(snapshot);
        onDataLoaded = delegate { };
    }

    private static void OnDatabaseLoaded()
    {
        onDatabaseLoaded();
        onDatabaseLoaded = delegate { };
    }

    public static void GetCurrentTime()
    {
        DebugLog("At start");

        //addMessage("HAHA").ContinueWith((task) => {
        //    if (task.IsFaulted)
        //    {
        //        foreach (var inner in task.Exception.InnerExceptions)
        //        {
        //            if (inner is FunctionsException)
        //            {
        //                var e = (FunctionsException)inner;
        //                // Function error code, will be INTERNAL if the failure
        //                // was not handled properly in the function call.
        //                var code = e.ErrorCode;
        //                var message = e.Message;

        //                DatabaseRuntime.Instance.AddEntry(delegate
        //                {
        //                    DebugLog("Code: " + code + " Meesage: " + message);
        //                });
        //            }
        //        }
        //    }
        //    else
        //    {
        //        string result = task.Result;
        //        DatabaseRuntime.Instance.AddEntry(delegate
        //       {
        //           DebugLog(result);
        //       });
        //    }
        //});

        getCurrentTime().ContinueWith((task) =>
        {
            DebugLog("continuewith1");

            if (task.IsFaulted)
            {
                DebugLog("task is faulted");

                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        var message = e.Message;
                        DebugLog("Code: " + code + " Meesage: " + message);
                    }
                }
            }
            else
            {
                DebugLog("not faulted");

                string result = task.Result;

                DebugLog(result);
            }
        });

        //FirebaseDatabase.DefaultInstance.GetReference("/server_values/ServerTime")
        //         .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
        //         {
        //             if (e2.DatabaseError != null)
        //             {
        //                 Debug.LogError(e2.DatabaseError.Message);

        //                 return;
        //             }

        //             if (e2.Snapshot != null)
        //             {

        //                 DebugLog("ServerTime : " + e2.Snapshot.Value.ToString());
        //             }
        //         };
    }

    private static Task<string> getCurrentTime()
    {
        DebugLog("inside task");

        //Call the function and extract the operation from the result. GetInstance("asia-east2")
        FirebaseFunctions function = FirebaseFunctions.GetInstance(FirebaseApp.DefaultInstance, "asia-east2");
        DebugLog("before callasync");

        return function.GetHttpsCallable("getservertime").CallAsync().ContinueWith((task) =>
        {
            DebugLog("inside callasync");

            return (string)task.Result.Data;
        });
    }

    private static Task<string> addMessage(string text)
    {
        // Create the arguments to the callable function.
        var data = new Dictionary<string, object>();
        data["text"] = text;
        data["push"] = true;

        // Call the function and extract the operation from the result.
        var function = FirebaseFunctions.DefaultInstance.GetHttpsCallable("addMessage");
        return function.CallAsync(data).ContinueWith((task) =>
        {
            return (string)task.Result.Data;
        });
    }

    #region Utilities

    public static string GetErrorMessage(AggregateException exception)
    {
        DebugLog(exception.ToString());
        FirebaseException firebaseEx = exception.InnerExceptions[0] as FirebaseException;
        if (firebaseEx != null)
        {
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
            return GetErrorMessage(errorCode);
        }

        return exception.ToString();
    }

    private static string GetErrorMessage(AuthError errorCode)
    {
        string message = "";
        switch (errorCode)
        {
            case AuthError.AccountExistsWithDifferentCredentials:
                message = "Account exists with different credentials";
                break;
            case AuthError.MissingPassword:
                message = "Missing password";
                break;
            case AuthError.WeakPassword:
                message = "Weak password";
                break;
            case AuthError.WrongPassword:
                message = "Wrong Password";
                break;
            case AuthError.EmailAlreadyInUse:
                message = "Email already in use";
                break;
            case AuthError.InvalidEmail:
                message = "Invalid email";
                break;
            case AuthError.MissingEmail:
                message = "Missing email";
                break;
            case AuthError.CredentialAlreadyInUse:
                message = "Credential is already in use";
                onCredentialAlreadyInUse();
                break;
            default:
                message = "An error occurred";
                break;
        }
        return message;
    }

    private static void DebugLog(string output)
    {
#if DEBUG_LOG
        Debug.Log(output);
#endif
    }

    #endregion
}
