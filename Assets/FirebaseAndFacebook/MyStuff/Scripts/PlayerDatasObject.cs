﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Player")]
public class PlayerDatasObject : ScriptableObject
{
    public PlayerDataInt playerID;
    public PlayerDataFloat playerExperience;
    public PlayerDataInt playerCoins;
    public PlayerDataInt playerGems;
    public PlayerDataCloths playerCloths;
    public PlayerDataInt playerStamina;

    public PlayerDatas ToRuntimeData()
    {
        PlayerDatas result = new PlayerDatas
        {
            playerID = this.playerID.property,
            playerCoins = this.playerCoins.property,
            playerGems = this.playerGems.property,
            playerExperience = this.playerExperience.property,
            playerCloths = this.playerCloths.property,
            playerStamina = this.playerStamina.property
        };

        return result;
    }
}

public class PlayerDatas
{
    public int playerID, playerCoins, playerGems, playerStamina;
    public float playerExperience;
    public Cloths playerCloths;

    public Dictionary<object, object> ToVData()
    {
        Dictionary<object, object> result = new Dictionary<object, object>();

        result.Add("playerID", playerID);
        result.Add("playerCoins", playerCoins);
        result.Add("playerGems", playerGems);
        result.Add("playerStamina", playerStamina);
        result.Add("playerExperience", playerExperience);
        result.Add("playerCloths", playerCloths.ToVData());

        return result;
    }

    public string GetRefPath()
    {
        return "PlayerDatas";
    }

    public PlayerDatas() { }
    public PlayerDatas(Dictionary<string, object> data)
    {
        this.playerID = int.Parse(data["playerID"].ToString());
        this.playerCoins = int.Parse(data["playerCoins"].ToString());
        this.playerGems = int.Parse(data["playerGems"].ToString());
        this.playerStamina = int.Parse(data["playerStamina"].ToString());
        this.playerExperience = float.Parse(data["playerExperience"].ToString());
        this.playerCloths = new Cloths(data["playerCloths"] as List<object>);
    }
}

