﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataController : MonoBehaviour
{
    private static PlayerDataController _instance;
    public static PlayerDataController Instance { get { return _instance; } }

    [SerializeField] private PlayerDatasObject samplePlayerDatas, defaultPlayerDatas;
    [SerializeField] private bool useSamplePlayer = false;

    private readonly string playerIDKey = "PlayerID";
    private readonly string defaultID = "";
    private readonly int startID = 10000;

    private PlayerDatas _runtimeDatas;
    public PlayerDatas RuntimeDatas { get { return _runtimeDatas; } }
    private string playerIDKeyString;
    private object playerDatas;

    private ActionQueue actionQueue;
    public static Action RuntimeDatasChanged = delegate { };

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        if (_instance == this)
        {
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        actionQueue = new ActionQueue();
        _runtimeDatas = new PlayerDatas();
        playerIDKeyString = SecuredPlayerPrefsN.GetString(playerIDKey, defaultID);

        //Database.Instance.onDatabaseLoaded += Initialization;

    }

    void Update()
    {
        actionQueue.UpdateQueue();
    }

    public void Initialization()
    {
        //  For testing
        if (useSamplePlayer && samplePlayerDatas != null)
        {
            _runtimeDatas = samplePlayerDatas.ToRuntimeData();
            playerIDKeyString = defaultID;
        }
        else
        {
            if (playerIDKeyString == defaultID)
            {
                //  No id, new player or uninstallled
                _runtimeDatas = defaultPlayerDatas.ToRuntimeData();
            }
            else
            {
                LoadPlayerData();
            }
        }

        Database.Instance.onDatabaseLoaded -= Initialization;
    }

    public void LoadPlayerData()
    {
        Database.Instance.onPlayerDataLoaded += LoadDataToRuntimeData;
        Database.Instance.LoadPlayerData();
    }

    private void LoadDataToRuntimeData(DataSnapshot snapshot)
    {
        if (snapshot.HasChild(playerIDKeyString))
        {
            playerDatas = snapshot.Child(playerIDKeyString).Value;

            _runtimeDatas = new PlayerDatas(playerDatas as Dictionary<string, object>);
            actionQueue.AddEntry(RuntimeDatasChanged);
        }
        else
        {
            DebugLog("No such player exist in database.");
        }
    }

    public void SavePlayerData()
    {
        bool isDefault = false;

        if (playerIDKeyString == defaultID)
        {
            isDefault = true;
            _runtimeDatas.playerID = startID + Database.Instance.PlayerCount;
        }

        SavePlayerPref(Database.Instance.SavePlayerData(isDefault, playerIDKeyString, RuntimeDatas.ToVData()));
    }

    private void SavePlayerPref(string databaseKeyString)
    {
        SecuredPlayerPrefsN.SetString(playerIDKey, databaseKeyString);
        playerIDKeyString = databaseKeyString;
    }

    public void LogIn()
    {
        FirebasePlugin.LoginAsAnon();
        Initialization();
    }

    private void DebugLog(string output)
    {
#if DEBUG_LOG
        Debug.Log(output);
#endif
    }

    public void RemovePlayerPerf()
    {
        PlayerPrefs.DeleteKey(playerIDKey);
    }

    public static void IncreaseInt(ref int variable, int value)
    {
        variable += value;
        RuntimeDatasChanged();
    }

    public static void IncreaseFloat(ref float variable, float value)
    {
        variable += value;
        RuntimeDatasChanged();
    }

    public static void ChangeBool(ref bool variable, bool value)
    {
        variable = value;
        RuntimeDatasChanged();
    }
}

public class ActionQueue<T>
{
    private class ActionPair<T>
    {
        public System.Action<T> act;
        public T arg;
    }

    private List<ActionPair<T>> pairs = new List<ActionPair<T>>();

    public void UpdateQueue()
    {
        if (pairs.Count > 0)
        {
            if (pairs[0] != null)
            {
                pairs[0].act(pairs[0].arg);
            }

            pairs.RemoveAt(0);
        }
    }

    public void AddEntry(System.Action<T> act, T arg)
    {
        pairs.Add(new ActionPair<T>() { act = act, arg = arg });
    }
}

public class ActionQueue
{
    private class ActionPair
    {
        public System.Action act;
    }

    private List<ActionPair> pairs = new List<ActionPair>();

    public void UpdateQueue()
    {
        if (pairs.Count > 0)
        {
            if (pairs[0] != null)
            {
                pairs[0].act();
            }

            pairs.RemoveAt(0);
        }
    }

    public void AddEntry(System.Action act)
    {
        pairs.Add(new ActionPair() { act = act });
    }
}