﻿using Firebase.Database;
using UnityEngine;

public class DatabaseRuntime : MonoBehaviour
{
    public static DatabaseRuntime Instance {  get { return _instance; } }
    private static DatabaseRuntime _instance;

    private ActionQueue myQueue = new ActionQueue();
    private ActionQueue<DataSnapshot> snapshotQueue = new ActionQueue<DataSnapshot>();
    private ActionQueue<string> stringQueue = new ActionQueue<string>();

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        if (_instance  == this)
        {
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        myQueue.UpdateQueue();
        snapshotQueue.UpdateQueue();
        stringQueue.UpdateQueue();
    }

    public void AddEntry(System.Action act)
    {
        myQueue.AddEntry(act); 
    }

    public void AddEntry(System.Action<DataSnapshot> act, DataSnapshot arg)
    {
        snapshotQueue.AddEntry(act, arg);
    }

    public void AddEntry(System.Action<string> act, string arg)
    {
        stringQueue.AddEntry(act, arg);
    }
}
