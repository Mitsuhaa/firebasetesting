﻿using System;
using System.Text;
using System.Security.Cryptography;
using UnityEngine;

public static class SecuredPlayerPrefsN
{
    private const char delimiter = '*';
    private const string privateKey = "422244422244422t";
    private static readonly string uniqueKey = SystemInfo.deviceUniqueIdentifier;
    private static readonly string secretKey = privateKey + uniqueKey;

    private static string hash;
    private static StringBuilder sb = new StringBuilder();
    private static StringBuilder hashSB = new StringBuilder();

    public static void SetString(string key, string value)
    {
        hashSB.Clear();
        hash = MD5Hash.GetMd5Hash(hashSB.Append(key).Append(value).Append(secretKey).ToString());

        SetHere(key, value.ToString(), hash);
    }

    public static void SetInt(string key, int value)
    {
        hashSB.Clear();
        hash = MD5Hash.GetMd5Hash(hashSB.Append(key).Append(value).Append(secretKey).ToString());

        SetHere(key, value.ToString(), hash);
    }

    public static void SetFloat(string key, float value)
    {
        hashSB.Clear();
        hash = MD5Hash.GetMd5Hash(hashSB.Append(key).Append(value).Append(secretKey).ToString());

        SetHere(key, value.ToString(), hash);
    }

    private static void SetHere(string key, string value, string hash)
    {
        sb.Clear();
        sb.Append(value).Append(delimiter).Append(hash);

        PlayerPrefs.SetString(key, sb.ToString());
    }

    public static string GetString(string key)
    {
        return GetString(key, "");
    }

    public static string GetString(string key, string defaultValue)
    {
        if (!PlayerPrefs.HasKey(key)) return defaultValue;

        string[] split = PlayerPrefs.GetString(key, defaultValue).Split(delimiter);

        if (split.Length != 2) return defaultValue;

        hashSB.Clear();
        if (MD5Hash.GetMd5Hash(hashSB.Append(key).Append(split[0]).Append(secretKey).ToString()) == split[1])
        {
            return split[0];
        }
        else
        {
            return defaultValue;
        }
    }

    public static int GetInt(string key, int defaultValue)
    {
        if (!PlayerPrefs.HasKey(key)) return defaultValue;

        string[] split = PlayerPrefs.GetString(key).Split(delimiter);

        if (split.Length != 2) return defaultValue;

        hashSB.Clear();
        if (MD5Hash.GetMd5Hash(hashSB.Append(key).Append(split[0]).Append(secretKey).ToString()) == split[1])
        {
            int result;
            if (int.TryParse(split[0], out result))
            {
                return result;
            }
            else
            {
                return defaultValue;
            }
        }
        else
        {
            return defaultValue;
        }
    }

    public static float GetFloat(string key, float defaultValue)
    {
        if (!PlayerPrefs.HasKey(key)) return defaultValue;

        string[] split = PlayerPrefs.GetString(key).Split(delimiter);

        if (split.Length != 2) return defaultValue;

        hashSB.Clear();
        if (MD5Hash.GetMd5Hash(hashSB.Append(key).Append(split[0]).Append(secretKey).ToString()) == split[1])
        {
            float result;
            if (float.TryParse(split[0], out result))
            {
                return result;
            }
            else
            {
                return defaultValue;
            }
        }
        else
        {
            return defaultValue;
        }
    }

    public static void Save()
    {
        PlayerPrefs.Save();
    }

    public static class MD5Hash
    {
        public static string GetMd5Hash(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        // Verify a hash against a string.
        public static bool VerifyMd5Hash(string input, string hash)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Hash the input.
                string hashOfInput = GetMd5Hash(input);

                // Create a StringComparer an compare the hashes.
                StringComparer comparer = StringComparer.OrdinalIgnoreCase;

                if (0 == comparer.Compare(hashOfInput, hash))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
